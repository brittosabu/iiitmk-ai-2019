import os
import sys
from pathlib import Path
import argparse

people = Path("./people")
agents = {}
for person in os.listdir(people):
    person = people / person
    scope = {}
    for pyfile in person.glob("*py"):
        with open(pyfile, "r") as fl:
            exec(fl.read(), scope)
    agents[person] = scope

# ------------------------------
parser = argparse.ArgumentParser()
parser.add_argument("world_file", help="Path to world simulation file")
args = parser.parse_args()
test_functions = {}
with open(args.world_file, "r") as fl:
    exec(fl.read(), test_functions)
for fn_name, fn in test_functions.items():
    prefix = "world"
    if fn_name.startswith(prefix):
        participant_agents = {
            str(person.name): agent_fn
            for person, scope in agents.items()
            for agent_name, agent_fn in scope.items()
            if agent_name.startswith(fn_name[len(prefix) + 1 :])
        }
        fn(**participant_agents)
