import random


def generate_road(rows=1, cols=5):
    vehicles = ["car", "bik", None]
    left = [[random.choice(vehicles) for _ in range(cols)] for _ in range(rows)]
    right = [[random.choice(vehicles) for _ in range(cols)] for _ in range(rows)]
    return left, right


def reference(left, right):
    return all([row[0] is None for row in right])


def test(should_cross):
    for _ in range(100):
        road = generate_road()
        got, ref = should_cross(*road), reference(*road)
        assert got == ref, f"left={road[0]}\nright={road[1]}\nexpected={ref}\ngot={got}"
