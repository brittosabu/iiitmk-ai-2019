# CDA2308: AI & Knowledge representation

Elective, Semester: III, Credits: 3

## Prerequisite

- Have access to a laptop/desktop
- You should have a text editor (vim/emacs/gedit/notepad++) or an IDE(gitlab web IDE/vscode/pycharm/jupyter)
	- Course instructors will only have support for simple text editor + [python 3.7](https://www.python.org/downloads/)
- Please go through this [gitlab web IDE tutorial](https://www.youtube.com/watch?v=Y2SsnCHJd2w)
	- you should know how to create a new branch
	- you should know how to submit a merge request

## Course Method

- We have one class each week on Wednesday
- Each Wednesday class has 2 parts
	- discussion on material/assignments issued on last class
	- new topic introduction
- We use [this gitlab project](https://gitlab.com/theSage21/iiitmk-ai-2019) for assignments/reading lists/other materials etc
- Reading lists will be supplied
	- Please read the article / watch the video/listen to the talk mentioned in the reading list before the next Wednesday class happens
	- Next Wednesday will consist of discussions on the material
- Assignments will be given
	- Submission for each assignment requires programs unless otherwise specified
	- Submissions must be made as Merge requests to this repository unless otherwise specified
- We might use the college's Moodle if regulation requires us to
- Marking scheme will be discussed in class


## Text Books

- Peter Norvig, Paradigms of Artificial Intelligence Programming: Case Studies in Common LISP, Morgan Kaufmann, 1992
- Ronald J. Brachman, Hector J. Levesque, Knowledge, Representation and Reasoning, The Morgan Kaufmann Series in Artificial Intelligence, 2004
- T.J.M. Bench-Capon, Knowledge Representation: An Approach to Artificial Intelligence, Elsevier, 2014 
- Stuart Jonathan Russell, Peter Norvig, Artificial Intelligence: A Modern Approach, Prentice Hall, 2010

## Syllabus/Study Plan


- [MODULE I](https://gitlab.com/gitcourses/iiitmk-ai-2019/-/milestones/1)
    - Introduction to AI
    - Intelligent Agents and Environment
    - Structure of Agents
    - Nature of Environments
    - Implication of Bigdata in AI
- [MODULE II](https://gitlab.com/gitcourses/iiitmk-ai-2019/-/milestones/2)
    - Solving Problem by Searching
    - Searching Agents
    - Seeking Agents - go beyond Search
    - Uninformed Search Strategies
    - Searching with Partial Information
    - Blind Search, Informed Search and Exploration 
    - Heuristic Search
    - Local Search
    - Constraint Satisfaction
    - Online Search
    - Adversial Search
    - Game theory
- [MODULE III](https://gitlab.com/gitcourses/iiitmk-ai-2019/-/milestones/3)
    - Knowledge and reasoning
    - Knowledge based Agents and Logical Agents
    - First Order Logic
    - Inference in First Order Logic
    - Uncertain Knowledge and Reasoning
    - Probabilistic Reasoning
- [MODULE IV](https://gitlab.com/gitcourses/iiitmk-ai-2019/-/milestones/4)
    - Planning and Decision making
    - Planning with state space search
    - Planning Graph
    - Planning and acting in the real world
    - Multi-Agent Planning
    - Making Simple & complex Decisions
- [MODULE V](https://gitlab.com/gitcourses/iiitmk-ai-2019/-/milestones/5)
    - Ontology in Knowledge Representation
    - Folksonomy and Web 2.0 / Web 3.0 technologies
    - Various Graph Databases for KR: Allegrograph, Hypergraph, Pregel, Trinity, Tao, FlockDB
    - Storage and Retrieval of Knowledge
    - Distributed Storage for Sparse Dataset
    - In - Memory Cache for Knowledge Representation and Trinity
    - Indexing Mechanism for Query Retrieval
    - Authorization and Authentication for Knowledge Access and Manipulation
